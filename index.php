<?
	include_once('employeeheader.php');

	if(isset($_POST['user'])){
		$username = filter($_POST['user']);
		$password = filter($_POST['pass']);

		$msg = 'User Name or Password is incorrect';

		if($username && $password){
			$sql = "select id, decode(fullname,'$key') fullname, admin,ip_addr from emp where username = encode('$username','$key') and password = encode('$password','$key')";
		
			$rs = $db->query($sql);

			if($rs->num_rows > 0){
				$emp = $rs->fetch_assoc();
				$emp['username'] = $username;
				$_SESSION['emp'] = $emp;
				header('Location: '. $url); exit;
			}
		}
	}

	if($emp['id'] > 0){
		$comments = '';
		$status = 'Absent';		
		$ip = $_SERVER['REMOTE_ADDR'];
		$sql = "select id, attDate, decode(inTime,'$key') inTime, decode(outTime,'$key') outTime, decode(inComments,'$key') inComments, decode(outComments,'$key') outComments from att where empId = $emp[id] and attDate = '$now'";
		$rs = $db->query($sql);

		if($rs->num_rows > 0){
			$att = $rs->fetch_assoc();

			if($att['inTime']){
				$status = 'Present';
			}

			if($att['outTime']){
				$status = 'Went';
			}
		}

		switch($status){
			case 'Absent':
				$attTitle = 'Clock In';
				$arrivalTime = '';
				$arrivalComments = '';
				$departureTime = '';
				$departureComments = '';
				break;
			case 'Present':
				$attTitle = 'Clock Out';
				$arrivalTime = date('h:i:s a', strtotime($att['inTime']));
				$arrivalComments = stripslashes(strip_tags($att['inComments']));
				$departureTime = '';
				$departureComments = '';
				break;
			case 'Went':
				$arrivalTime = date('h:i:s a', strtotime($att['inTime']));
				$arrivalComments = stripslashes(strip_tags($att['inComments']));
				$departureTime = date('h:i:s a', strtotime($att['outTime']));
				$departureComments = stripslashes(strip_tags($att['outComments']));
		}

		if($status != 'Went' && isset($_POST['com'])){
			switch($status){
				case 'Absent':
					$inTime = date('H:i:s');
					$inComments = htmlentities(filter($_POST['com']));
					$msg = 'Your clock in time has been locked.';
					
					if($att){
						$sql = "update att set inIp = '$ip', attDate = '$now', empId = $emp[id], inTime = encode('$inTime','$key'), inComments = encode('$inComments','$key') where id = $att[id]";
					}
					else{
						$sql = "insert into att set inIp = '$ip', attDate = '$now', empId = $emp[id], inTime = encode('$inTime','$key'), inComments = encode('$inComments','$key')";
					}
					
					break;
				case 'Present':
					$outTime = date('H:i:s');
					$outComments = htmlentities(filter($_POST['com']));
					$msg = 'Your clock out time has been locked.';
					$sql = "update att set outIp = '$ip', attDate = '$now', empId = $emp[id], outTime = encode('$outTime','$key'), outComments = encode('$outComments','$key') where id = $att[id]";
			}
			
			if($sql) $db->query($sql);
		}
	}
?>
<form class="form-horizontal" method=post>

<? if($emp['id'] > 0){ ?>
	
	<style>
.errorstar{color:#dd4b39;}
</style>
	<div class="content-wrapper">
<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
			Attendance
			</h1>
			<ol class="breadcrumb">
			<li><a href="<?= SITE_URL ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Attendance</li>
			</ol>
		</section>
		<section class="content">
    <div class="row">
        <div class="col-md-10">
         
		<div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Attendance</h3>
                </div>
			<? if($msg){ ?>
			  <div class="alert alert-success">
					 <?= $msg ?>
				</div>
			   	<input type=button value="Go to Attendance Page" onclick="location.href='<?= $url ?>'" class="btn btn-info pull-left">
			   <? }else{ ?>
			     <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Attendance Date</label>

                            <div class="col-sm-10">
                              <label for="name" class="control-label"> <?= _date_format($now) ?></label>
                            </div>
                        </div>
                    </div>
						<? if($arrivalTime){ ?>
				 <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Clock In Time</label>

                            <div class="col-sm-10">
                            <label for="name" class="control-label">   <?= $arrivalTime ?> </label>
                            </div>
                        </div>
                    </div>
			   <? }?>
			   	<? if($departureTime){ ?>
				 <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Clock Out Time</label>

                            <div class="col-sm-10">
                                <label for="name" class="control-label"> <?= $departureTime ?></label>
                            </div>
                        </div>
                    </div>
				<? } ?>
				<? if($status == 'Absent'){ ?>
				 <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Clock In Comments (optional)</label>

                            <div class="col-sm-10">
                             <textarea name=com  class="form-control" row="4" style="height:200px"></textarea>
                            </div>
                        </div>
                    </div>
				<? } ?>
				<? if($status == 'Present'){ ?>
				 <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Mini Report </label>

                            <div class="col-sm-10">
                             <textarea name=com  row="4" class="form-control" style="height:200px"></textarea>
                            </div>
                        </div>
                    </div>
				<? } ?>
				<? if($status != 'Went'){ ?>
				<div class="box-footer">
                       
                      <input type=submit value="Submit <?= $attTitle ?> Time" onclick="return confirm('You are going to <?=strtoupper($attTitle)?>. Do you really want to continue?')" class="btn btn-info pull-left">
                    </div>
					
					<? } ?>
					<? } ?>	
                
        </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<? }else{ ?>

	<?php include('login.php');?>
	
	
<?	} ?>
</form>
<script src="admin_theme/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="admin_theme/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="admin_theme/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="admin_theme/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="admin_theme/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="admin_theme/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="admin_theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="admin_theme/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="admin_theme/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="admin_theme/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="admin_theme/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="admin_theme/dist/js/demo.js"></script>
</body>
</html>
