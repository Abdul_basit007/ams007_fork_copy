<?
	error_reporting(E_ALL & ~E_NOTICE);
	include_once('config.php');
	session_start();
	$db = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	$url = SITE_URL;
	$msg = '';
	$key = "dGFo";
	$now = date('Y-m-d');
	
	if(isset($_GET['logout'])){
		session_destroy();
		header('Location: '. $url); exit;
	}

	$emp = isset($_SESSION['emp']) ? $_SESSION['emp'] : array('id'=>0,'username'=>'','fullname'=>'');
		

	function loginCheck(){
		global $url;	

		if(!isset($_SESSION['emp'])){
			header('Location: '.$url); exit;
		}
	}
	
	function db_scalar($sql){
		global $db;
		$r = 0;
		
		if($rs = $db->query($sql)){
			$row = $rs->fetch_row();
			$r = $row[0];
		}
		
		return $r;
	}
	
	function _date_format($d){
		$d = is_int($d) ? $d : strtotime($d);
		return date('l, d F, Y', $d);
	}
	
	function _time_format($t){
		$t = is_int($t) ? $t : strtotime($t);
		return date('h:i:s a', $t);
	}
	
	function _time_diff($start_time="2013-10-20 08:10:00",$end_time="2013-10-20 09:10:01"){
	
		$difference = "DULL Person";
		
		if($start_time && $end_time){
			$date_a = new DateTime($start_time);
			$date_b = new DateTime($end_time);
			
			$date_a = $date_a->format("U");
			$date_b = $date_b->format("U");
			
			//$interval = date_diff($date_b,$date_a);
			$rv = ($date_b - $date_a);
			$interval = new DateInterval1($rv);
			
			$hours = $interval->h;
			$minutes = $interval->i;
		
			$hours = $hours + ($interval->d*24);

			$difference = $hours . ":" . $minutes;
			//echo $difference;
		}
		
		return $difference;
	}
	
	function filter($str){
		return get_magic_quotes_gpc() ? trim($str) : addslashes(trim($str)); 
	}
	function diff ($secondDate){
		$firstDateTimeStamp = $this->format("U");
		$secondDateTimeStamp = $secondDate->format("U");
		$rv = ($secondDateTimeStamp - $firstDateTimeStamp);
		$di = new DateInterval1($rv);
		return $di;
	}
	
	Class DateInterval1 {
		/* Properties */
		public $y = 0;
		public $m = 0;
		public $d = 0;
		public $h = 0;
		public $i = 0;
		public $s = 0;

		/* Methods */
		public function __construct ( $time_to_convert /** in seconds */) {
			$FULL_YEAR = 60*60*24*365.25;
			$FULL_MONTH = 60*60*24*(365.25/12);
			$FULL_DAY = 60*60*24;
			$FULL_HOUR = 60*60;
			$FULL_MINUTE = 60;
			$FULL_SECOND = 1;

	//        $time_to_convert = 176559;
			$seconds = 0;
			$minutes = 0;
			$hours = 0;
			$days = 0;
			$months = 0;
			$years = 0;

			while($time_to_convert >= $FULL_YEAR) {
				$years ++;
				$time_to_convert = $time_to_convert - $FULL_YEAR;
			}

			while($time_to_convert >= $FULL_MONTH) {
				$months ++;
				$time_to_convert = $time_to_convert - $FULL_MONTH;
			}

			while($time_to_convert >= $FULL_DAY) {
				$days ++;
				$time_to_convert = $time_to_convert - $FULL_DAY;
			}

			while($time_to_convert >= $FULL_HOUR) {
				$hours++;
				$time_to_convert = $time_to_convert - $FULL_HOUR;
			}

			while($time_to_convert >= $FULL_MINUTE) {
				$minutes++;
				$time_to_convert = $time_to_convert - $FULL_MINUTE;
			}

			$seconds = $time_to_convert; // remaining seconds
			$this->y = $years;
			$this->m = $months;
			$this->d = $days;
			$this->h = $hours;
			$this->i = $minutes;
			$this->s = $seconds;
		}
	}
	
	
?>
<html>
<head>
<title>QuaidTech - QT Labs -  Attendance Management System</title>
<link href=style.css rel=stylesheet type="text/css">
<link href=tinymce.css rel=stylesheet type="text/css">
<script type="text/javascript" src="jquery-1.7.1.min.js"></script>
</head>
<body>	
	<table cellpadding=5 cellspacing=0 width=90% class=cont>
		<? if($emp['id'] > 0){ ?>
		<tr>
			<th class=menu align=left>
				<a href="<?= SITE_URL ?>">Attendance</a> |
				<a href="<?= SITE_URL.'calendar.php' ?>">Calendar</a><!--  |
				<a href="<?= SITE_URL.'tasks.php' ?>">Daily Notes/Tasks</a> -->
				|<? if($emp['admin']){ ?>
					<a href="<?= SITE_URL.'employees.php' ?>">Employee Management</a>
				<? } ?>
			</th>
			<th class=menu align=right>
				<a>Welcome <?= $emp['fullname'] ?></a>
				| <a href="<?= SITE_URL.'?logout' ?>">Logout</a>
			</th>
		</tr>			
		<? } ?>
		<tr><td colspan=2 align=left>
