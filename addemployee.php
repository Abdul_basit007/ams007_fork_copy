<?php
	include_once('employeeheader.php');
		if(!$emp['admin'])
		{
			header('Location: calendar.php');
		}
?>
<?php
 if(isset($_POST['addsub']))
{
	$fullname =$_POST['fullname'] ;
	$username =$_POST['username'] ;
	$email =$_POST['email'] ;
	$password =$_POST['password'] ;
	
	
	
	$msg ='';
	$sql="INSERT INTO emp (fullname,username,email,password)
           VALUES(ENCODE('$fullname', '$key'),ENCODE('$username', '$key'),ENCODE('$email', '$key'),ENCODE('$password', '$key'))";
	$rs = $db->query($sql);
	if($rs)
	{
		header('Location: employees.php?msg=add');
	}
	
}

?>
<style>
.errorstar{color:#dd4b39;}
</style>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Employee

       </h1>
      <ol class="breadcrumb">
        <li><a href="<?= SITE_URL ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= SITE_URL.'employees.php' ?>">Employees</a></li>
        <li class="active">Add Employee</li>
      </ol>
    </section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Employee</h3>
                </div>
				
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" role="form" method="POST" action="">
                  

                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Employee Name <span class="errorstar">*</span></label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="Employee Name" id="fullname" name="fullname" required>
                            </div>
                        </div>
                    </div>
					<div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">User Name <span class="errorstar">*</span></label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="User Name" id="username" name="username" required>
                            </div>
                        </div>
                    </div>
					<div class="box-body">
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email <span class="errorstar">*</span></label>

                            <div class="col-sm-10">
                                <input type="email" class="form-control" placeholder="Email" id="email" name="email" required>
                            </div>
                        </div>
                    </div>
					<div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Password <span class="errorstar">*</span></label>

                            <div class="col-sm-10">
                                <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
                            </div>
                        </div>
                    </div>
                   <div class="box-footer">
                        
                        <button type="submit" name="addsub" class="btn btn-info pull-right">Create</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
    <!-- Main content -->
   
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="admin_theme/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="admin_theme/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="admin_theme/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="admin_theme/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="admin_theme/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="admin_theme/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="admin_theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="admin_theme/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="admin_theme/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="admin_theme/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="admin_theme/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="admin_theme/dist/js/demo.js"></script>
</body>
</html>
