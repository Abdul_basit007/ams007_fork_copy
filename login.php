<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    QuaidTech-AMS
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
  <? if($msg){ ?>
    <p class="login-box-msg" style="color:red;"><?= $msg ?></p>
  <? } ?>
    <form action="admin_theme/index2.html" method="post">
      <div class="form-group has-feedback">
		<input type="text" name="user" value="<?= $emp['username'] ?>" class="form-control" placeholder="User Name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
   
		<input type="password" name="pass" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
</div>
  <!-- /.login-box-body -->
</div>
</body>

