<?
	error_reporting(0);
	include_once('config.php');
	session_start();
	$db = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	$url = SITE_URL;
	$msg = '';
	$key = "dGFo";
	
	if(isset($_SESSION['emp'])){
		$emp = $_SESSION['emp'];
	}
	else{
		header('Location: '.$url); exit;
	}
	
	$year = isset($_GET['y']) ? intval($_GET['y']) : date('Y');
	$month = isset($_GET['m']) ? intval($_GET['m']) : date('m');
	
	$calView = $_REQUEST['calView'];
	
	if($calView && $calView != 1){
		ECHO "Naughty! We are noting what you do. Don't do it again otherwise... <br><br><br><br> no problem, do it again to waste your time...";exit;
	}	
	
	if($emp['admin']){
		$empIdg = isset($_GET['e']) ? intval($_GET['e']) : $emp['id'];
		$empId = $empIdg;
		$emps = array();
		$sql = "select id, decode(fullname, '$key') fullname from emp order by decode(fullname, '$key') asc";
		$rs = $db->query($sql);
		while($row = $rs->fetch_assoc()) $emps[] = $row;
	}
	else $empId = $emp['id'];
	
	$months = array();
	$sql = "select distinct month(attDate) month from att where empId = $empId";
	$rs = $db->query($sql);
	while($rs && ($row = $rs->fetch_assoc())) $months[] = $row;
	$years = array();
	$sql = "select distinct year(attDate) year from att where empId = $empId";
	$rs = $db->query($sql);
	while($rs && ($row = $rs->fetch_assoc())) $years[] = $row;
		
	$sql = "select decode(fullname,'$key') fullname from emp where id = $empId";
	$rs = $db->query($sql);
	$row = $rs ? $rs->fetch_assoc() : array('fulname'=>'');
	$empFullname = $row['fullname'];
	
	$holidays = array();
	$holidaysQuery = "SELECT  decode(holidayDate,'$key') as holidayDate,  decode(holidayTitle,'$key') as holidayTitle FROM holidays";
	$rs = $db->query($holidaysQuery);
	while($rs && ($row = $rs->fetch_assoc())){
		$holidays[] = $row;
	}
	
	$sql = "select inIp, outIp, attDate, decode(inTime,'$key') inTime, decode(outTime,'$key') outTime, decode(inComments,'$key') inComments, decode(outComments,'$key') outComments
			from att
			where year(attDate) = '$year' and month(attDate) = '$month' and empId = $empId";
	$rs = $db->query($sql);
	$atts = array();
	while($rs && ($row = $rs->fetch_assoc()))
	{
		$atts[] = $row;
	}
	function filter($str){
		return get_magic_quotes_gpc() ? trim($str) : addslashes(trim($str)); 
	}
?>
<html>
<head>
<title>PakCyber Attendance</title>
<style>
	body, table{font-size:15px;font-family:"comic sans ms";background:#cccccc;color:#ffffff}
	.msg{color:red}
	td,th{text-align:left}
	textarea{width:400px;height:50px}
	.cont{background:#567378;margin:30px auto;padding:15px;border:2px solid #2B3651}
	.tabu{background:#567378;border-collapse:collapse}
	.com{color:#ffff00;font-size:13px}
	.comment{text-decoration:underscore;}
	.menu a{color:#ffffff;font-size:20px}
	.box td, .box th{border:1px solid #cccccc}
	.link{color:#ffffff;font-weight:bold; }
/* calendar */
table.calendar    { background:#fff;color:#000;border-left:1px solid #999; }
tr.calendar-row  {  }
td.calendar-day  { min-height:80px; font-size:11px; position:relative; } * html div.calendar-day { height:80px; }
td.calendar-day:hover  { background:#eceff5; }
td.calendar-day-np  { background:#eee; min-height:80px; } * html div.calendar-day-np { height:80px; }
td.calendar-day-head { background:#ccc; font-weight:bold; text-align:center; width:120px; padding:5px; border-bottom:1px solid #999; border-top:1px solid #999; border-right:1px solid #999; }
div.day-number    { background:#999; padding:5px; color:#fff; font-weight:bold; float:right; margin:-5px -5px 0 0; width:20px; text-align:center; }
/* shared */
td.calendar-day, td.calendar-day-np { width:120px; padding:5px; border-bottom:1px solid #999; border-right:1px solid #999; }
.red{color:red;font-size:13px;font-weight:bold;}
.redplus{color:red;font-size:13px;font-weight:bold;background:#f6f6f6}
.green{color:green;font-size:13px;font-weight:bold;}
.blue{color:blue;font-size:13px;font-weight:bold;}
.orange{color:orange;font-size:13px;font-weight:bold;}
.mrwhite{background:#fff;font-size:13px;font-weight:bold;color:black;}
.mrbold{background:yellow;font-size:15px;font-weight:bold;color:red;}
.mrnormal{}
</style>
</head>
<body>
	<form method=get>
		<table cellpadding=5 cellspacing=0 width=90% class=cont>
			<tr><td>
				<table cellpadding=5 cellspacing=0 width=95% class=tabu>
					<tr>
						<th colspan=5 class=menu>
							<a href="<?= SITE_URL ?>">Attendance</a>|
							<a href="<?= SITE_URL.'att.php' ?>">Report</a>
						</th>
					</tr>
					<tr>
						<td colspan=5>
							<? if($emps){ ?>
							<select name=e onchange="location.href='<?= SITE_URL.'att.php?e=' ?>'+this.value">
							<? foreach($emps as $e){ ?>
							<option value="<?= $e['id'] ?>" <? if($e['id'] == $empIdg) echo 'selected' ?>><?= $e['fullname'] ?></option>
							<? } ?>
							</select>
							<? } ?>

							<? if($years){ ?>
							<select name=y onchange="this.form.submit()">
							<? foreach($years as $y){ ?>
							<option value="<?= $y['year'] ?>" <? if($y['year'] == $year) echo 'selected' ?>><?= $y['year'] ?></option>
							<? } ?>							
							</select>
							<? } ?>
							
							<? if($months){ ?>
							<select name=m onchange="this.form.submit()">
							<? foreach($months as $m){ ?>
							<option value="<?= $m['month'] ?>" <? if($m['month'] == $month) echo 'selected' ?>><?= $m['month'] ?></option>
							<? } ?>
							</select>
							<? } ?>
							
						</td>
					</tr>					
					<tr>
						<th colspan=4>Employee Name : <?= $empFullname ?>  </th>
						<td  align="right" style="text-align:right; border: 0px solid yellow;">
						<? if ($calView == 1) { ?><a href="att.php?year=<?=$year?>&m=<?=$month?>&e=<?=$empId?>&calView=0" class="link">Switch to Table View</a><? } 
						else { ?><a href="att.php?year=<?=$year?>&m=<?=$month?>&e=<?=$empId?>&calView=1" class="link">Switch to Calendar View</a><?}?> </td>
					</tr>
					
					<? if ($calView == 1) { ?>
					<tr><td colspan=5 >	
					<?	echo drawCalendar(intval($month),intval($year));  ?>
					</td></tr>
					<? }  
					else { 
						if($atts){ ?>
					<tr><td>	
					<tr class=box>
						<th width=35%>Date</th>
						<th>Clock In Time</th>
						<? if($emp['admin']){ ?>
						<th>Clock Out Time</th>
						<? } else { ?>
						<th colspan="3">Clock Out Time</th>
						<? } ?>
						<? if($emp['admin']){ ?>
						<th>Clock In IP</th>
						<th>Clock Out IP</th>
						<? } ?>
					</tr>
					<?   foreach($atts as $att){ ?>
					<tr class=box>
						<td><?=  date('l, d F, Y',strtotime($att['attDate'])); ?></td>
						<td><?= date('h:i:s a',strtotime($att['inTime'])); ?>
						<? if ($att['inComments']) { ?><a title="<?= $att['inComments']; ?>"><span class=com>(reason)</span></a><? } ?>
						
						</td>
						<? if($emp['admin']){ ?>
						<td>
						<? if ($att['outTime']) { ?><?= date('h:i:s a',strtotime($att['outTime'])); ?><? } ?>
						<? if ($att['outComments']) { ?><a title="<?= $att['outComments']; ?>"><span class=com>(reason)</span></a><? } ?></td>
						<? } else { ?>
						<td colspan="3">									
						<? if ($att['outTime']) { ?><?= date('h:i:s a',strtotime($att['outTime'])); ?><? } ?>
						<? if ($att['outComments']) { ?><a title="<?= $att['outComments']; ?>"><span class=com>(reason)</span></a><? } ?></td>
						<? } ?>			
						</td>
						<? if($emp['admin']){ ?>
						<td><?= $att['inIp'] ?></td>
						<td><?= $att['outIp'] ?></td>
						<? } ?>
					</tr>
					<? }}} ?>
					<? if($emp['admin'] or true){ ?>
					<tr>
						<td colspan=5 class="mrwhite">
						<span class="mrnormal">If you come later then available limits without any genuine reason, then your contribution to "Tea Party" will be as given below.</span><br>
						<span class="mrbold">Available Limits:</span>
						<br>
						<span>Before: 9:15 (Dude you are punctual and in time, nice to meet you! You are invited to "Tea Party" without any contribution.</span><br>
						<span class="green">Between: 9:16 to 9:30 (Green Zone - Allowed Limits 10 days) <br>You are little late so in green zone. You are allowed to be in green zone for 10 days. 11th day will be counted as penalty.</span><br>
						<span class="blue">Between: 9:31 to 9:45 (Blue Zone - Allowed Limits 5 days) <br>You are late but still you have intension to come early, so you are in blue zone. You are allowed to be in blue zone for 5 days. 6th day will be counted as penalty.</span><br>
						<span class="red">Between: 9:46 to 10:00 (Red Zone - Allowed Limits 3 days)<br>You are used to late frequently, bad  habit, you are now in red zone. You are allowed to be in red zone for 3 days. 4th day will be counted as penalty.</span><br>
						<span class="redplus">After: 10:AM (Red++ Zone - Allowed Limits 1 day)<br>You are the one we are finding. i.e. big contributor to our "Tea Party"... penalty will be decided by management at its own discretion, that would not be less then accumulative of all above in any case. </span><br>
						<span>Absent: You can take leave by informing management, no problem. If you are absent other then taking leave i.e. forgot to clock in, then same rules will be applied as mentioned in red++ zone.
						<span>All untold holidays will also be treated as a red++ mark.<br>
						<span class="mrbold">Contribution to "Tea Party":  RS: 100 Per Penalty Day!!! </span><br>
						</td>
					</tr>	
					<? } ?>
					<tr>
						<td colspan=3><input type=button value="Logout" onclick="location.href='<?= SITE_URL.'?logout' ?>'"></td>
					</tr>
				</table>			
			</td></tr>
		</table>
	</form>
</body>
</html>
<?php

function drawCalendar($month,$year){
	global $atts,$holidays;
   $matchYear  = substr($year,2,2);
   
   if($month < 10)
	$matchMonth = "0" . $month;
   else
	$matchMonth = $month;
	
   //print $matchYear . "---". $matchMonth;
   
  /* draw table */
  $calendar = '<table cellpadding="0" cellspacing="0" width="100%" class="calendar">';

  /* table headings */
  $headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
  $calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

  /* days and weeks vars now ... */
  $running_day = date('w',mktime(0,0,0,$month,1,$year));
  $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
  $days_in_this_week = 1;
  $day_counter = 0;
  $dates_array = array();

  /* row for week one */
  $calendar.= '<tr class="calendar-row">';

  /* print "blank" days until the first of the current week */
  for($x = 0; $x < $running_day; $x++):
    $calendar.= '<td class="calendar-day-np">&nbsp;</td>';
    $days_in_this_week++;
  endfor;

  /* keep going with days.... */
  for($list_day = 1; $list_day <= $days_in_month; $list_day++):
    $calendar.= '<td class="calendar-day">';
      /* add in the day number */
      $calendar.= '<div class="day-number">'.$list_day.'</div>';
	  if($list_day < 10)
		$matchDay = "0" . $list_day;
	  else
		$matchDay = $list_day;

	$matchDate = $matchYear . "-" . $matchMonth . "-" . $matchDay;
	/** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
	
	$displayDetails = $displayIn = $displayOut = "";
	$showColor = "";
	$totalRec = count($atts);
	for($i=0; $i<=$totalRec; $i++){
		if($atts[$i]['attDate'] == $matchDate){
			$displayDetails = $atts[$i]['attDate'];
			//print $atts[$i]['inComments'];
			//print $atts[$i]['inTime'] . "---" . $atts[$i]['outTime'] . "<br>";
			
			if($atts[$i]['inTime'])
				$showColor = timeSlot($atts[$i]['attDate'], $atts[$i]['inTime']);
				
			if ($atts[$i]['inComments']) { 
				$displayIn = "<a title=\"{$atts[$i]['inComments']}\"><u>" . date('h:i',strtotime($atts[$i]['inTime']))  .  "</u></a>";
			}
			else{
				$displayIn = date('h:i',strtotime($atts[$i]['inTime'])) ;
			}
			
			if($atts[$i]['outTime'])
				$dateShow = date('h:i',strtotime($atts[$i]['outTime']));
			else
				$dateShow = "";
				
			if ($atts[$i]['outComments']) { 
				$displayOut = "<a title=\"{$atts[$i]['outComments']}\"><u>" . $dateShow .  "</u></a>";
			}
			else{
				$displayOut = $dateShow;
			}
			$displayDetails =  $displayIn . " - " . $displayOut . "<br>"  ;
			break;
		}
		else{
			if($days_in_this_week == 7 or $days_in_this_week == 1)
				$displayDetails = "--";	
			else
				if ($matchDate <= date("y-m-d")){
					$totalHolidays = count($holidays);
					$displayDetails = "<font color='orange'><b>ABSENT</b></font>";
					for($j=0; $j<=$totalHolidays;$j++) {
						if($holidays[$j]['holidayDate'] == $matchDate){
								$displayDetails = "<font color='orange'><b>Holiday <br> {$holidays[$j]['holidayTitle'] }</b></font>";
						}
					}
				}	
				else
					$displayDetails = "";
		}
	}
    $calendar.= str_repeat("<p><span {$showColor}>".$displayDetails . '</span></p>',1);
      
    $calendar.= '</td>';
    if($running_day == 6):
      $calendar.= '</tr>';
      if(($day_counter+1) != $days_in_month):
        $calendar.= '<tr class="calendar-row">';
      endif;
      $running_day = -1;
      $days_in_this_week = 0;
    endif;
    $days_in_this_week++; $running_day++; $day_counter++;
  endfor;

  /* finish the rest of the days in the week */
  if($days_in_this_week < 8):
    for($x = 1; $x <= (8 - $days_in_this_week); $x++):
      $calendar.= '<td class="calendar-day-np">&nbsp;</td>';
    endfor;
  endif;

  /* final row */
  $calendar.= '</tr>';

  /* end the table */
  $calendar.= '</table>';
  
  /* all done, return result */
  return $calendar;
}

function timeSlot($dateIn, $timeIn){
	$formatDateIn = date("Y-m-d",strtotime($dateIn));
	$compareTimeIn = strtotime($formatDateIn . " " . $timeIn);

	$red   = strtotime($formatDateIn . " 09:46:00");
	$blue  = strtotime($formatDateIn . " 09:31:00");
	$green = strtotime($formatDateIn . " 09:16:00");
	
	if ($compareTimeIn - $red > 0)
		return "class=red";
	
	if ($compareTimeIn - $blue > 0)
		return "class=blue";
		
	if ($compareTimeIn - $green > 0)
		return "class=green";
	
	return "";
}

?>
