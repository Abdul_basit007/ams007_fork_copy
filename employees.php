<?php
	include_once('employeeheader.php');
		if(!$emp['admin'])
		{
			header('Location: calendar.php');
		}
?>
<?php
		$mymsg = '';
		if(isset($_GET['msg']))
		{
			$msg = $_GET['msg'];
			if($msg == 'add')
			{
				$mymsg = 'Employee has been created successfully.';
			}
			elseif($msg == 'delete')
			{
				$mymsg = 'Employee has been deleted successfully.';
			}
			elseif($msg == 'update')
			{
				$mymsg = 'Employee has been updated successfully.';
			}
		}
		$emps = array();
		$sql = "select id, decode(fullname, '$key')fullname,decode(username,'$key')username,decode(email,'$key')email,decode(password,'$key')password,ip_addr from emp order by username ASC";
		$rs = $db->query($sql);
		while($row = $rs->fetch_assoc()) $emps[] = $row;
		
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Employees Management
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= SITE_URL ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Employees</li>
      </ol>
    </section>
				<?php if($msg){?>
				<div class="alert alert-success">
					<strong>Success!</strong> <?php echo $mymsg ; ?>
					<span style="float:right;"><a href="<?= SITE_URL.'employees.php' ?>"><span class="glyphicon glyphicon-remove"></span></a><span>
				</div>
				<?php }?>
    <!-- Main content -->
    <section class="content">
	<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Employees</h3>
			</div>
			<div class="box-header">
                    <div class="col-xs-2" style="padding-left:0px;">
                        
						<a href="<?= SITE_URL.'addemployee.php' ?>" class="btn btn-block btn-primary"> Add New Employee</a>
                        <br>
                    </div>
                    <div class="col-xs-10">&nbsp;</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover table-responsive">
                <tr>
                  <th>Name</th>
                  <th>User Name</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Action</th>
                 </tr>
				 <?php for($i=0;$i<count($emps);$i++){?>
				<tr>
                  <td><?php echo $emps[$i]['fullname'] ?></td>
                   <td><?php echo $emps[$i]['username'] ?></td>
                   <td><?php echo $emps[$i]['email'] ?></td>
                   <td>
				   <span id="origional_<?php echo $emps[$i]['id']; ?>" style="display:none;"><?php echo $emps[$i]['password'] ?></span>
				   <span id="protect_<?php echo $emps[$i]['id']; ?>">*******</span>
				   <span id="eyeopen_<?php echo $emps[$i]['id']; ?>"><a href="javascript:void(0)" onclick="viewpassword(<?php echo $emps[$i]['id']; ?>);"/><i class="fa fa-eye" aria-hidden="true"></i></a></span>
				   
				   
				   <span id="eyeoclose_<?php echo $emps[$i]['id']; ?>" style="display:none;"><a href="javascript:void(0)"   onclick="hidepassword(<?php echo $emps[$i]['id']; ?>);"/><i class="fa fa-eye" aria-hidden="true"></i></a></span>
					
				   </td>
                   <td>
					<a href="detailemployee.php?e=<?php echo $emps[$i]['id'] ;?>"><button class="btn btn-success"><span class="glyphicon glyphicon-eye-open"></span> Details</button></a>
					<a href="editemployee.php?id=<?php echo $emps[$i]['id'] ;?>"><button class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Edit</button></a>
					<a href="delete.php?id=<?php echo $emps[$i]['id'] ;?>" onClick = "javascript: return confirm('Are you sure you wish to do this?');"><button class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</button></a></td>
                </tr>
				 <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script>
function viewpassword(value)
{
	$("#origional_"+value).show(); 
	$("#protect_"+value).hide(); 
	$("#eyeopen_"+value).hide(); 
	$("#eyeoclose_"+value).show(); 
}
function hidepassword(value)
{
	$("#origional_"+value).hide(); 
	$("#protect_"+value).show(); 
	$("#eyeopen_"+value).show(); 
	$("#eyeoclose_"+value).hide(); 
}
</script>
<!-- jQuery 2.2.3 -->
<script src="admin_theme/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="admin_theme/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="admin_theme/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="admin_theme/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="admin_theme/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="admin_theme/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="admin_theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="admin_theme/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="admin_theme/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="admin_theme/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="admin_theme/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="admin_theme/dist/js/demo.js"></script>
</body>
</html>
