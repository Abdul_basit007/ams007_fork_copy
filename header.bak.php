<?
	error_reporting(E_ALL & ~E_NOTICE);
	include_once('config.php');
	session_start();
	$db = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	$url = SITE_URL;
	$msg = '';
	$key = "dGFo";
	$now = date('Y-m-d');
	
	if(isset($_GET['logout'])){
		session_destroy();
		header('Location: '. $url); exit;
	}

	$emp = isset($_SESSION['emp']) ? $_SESSION['emp'] : array('id'=>0,'username'=>'','fullname'=>'');
		
	function loginCheck(){
		global $url;
		
		if(!isset($_SESSION['emp'])){
			header('Location: '.$url); exit;
		}
	}
	
	function db_scalar($sql){
		global $db;
		$r = 0;
		
		if($rs = $db->query($sql)){
			$row = $rs->fetch_row();
			$r = $row[0];
		}
		
		return $r;
	}
	
	function _date_format($d){
		$d = is_int($d) ? $d : strtotime($d);
		return date('l, d F, Y', $d);
	}
	
	function _time_format($t){
		$t = is_int($t) ? $t : strtotime($t);
		return date('h:i:s a', $t);
	}
	
	function filter($str){
		return get_magic_quotes_gpc() ? trim($str) : addslashes(trim($str)); 
	}
?>
<html>
<head>
<title>PakCyber Attendance</title>
<link href=style.css rel=stylesheet type="text/css">
<link href=tinymce.css rel=stylesheet type="text/css">
<script type="text/javascript" src="jquery-1.7.1.min.js"></script>
</head>
<body>	
	<table cellpadding=5 cellspacing=0 width=90% class=cont>
		<? if($emp['id'] > 0){ ?>
		<tr>
			<th class=menu align=left>
				<a href="<?= SITE_URL ?>">Attendance</a> |
				<a href="<?= SITE_URL.'calendar.php' ?>">Calendar</a> |
				<a href="<?= SITE_URL.'tasks.php' ?>">Daily Notes/Tasks</a>
				<? if(false && $emp['admin']){ ?>
					<a href="<?= SITE_URL.'tasks_assign.php' ?>">Assign Daily Tasks</a>
				<? } ?>
			</th>
			<th class=menu align=right>
				<a>Welcome <?= $emp['fullname'] ?></a>
				| <a href="<?= SITE_URL.'?logout' ?>">Logout</a>
			</th>
		</tr>			
		<? } ?>
		<tr><td colspan=2 align=left>
